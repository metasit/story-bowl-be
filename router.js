
const controllerLogin = require('./controller/login/')
const controllerRegister = require('./controller/register')
const controllerProject  = require('./controller/project/project_home')
const controllerBoard = require('./controller/board/board_home')
const controllerPremise = require('./controller/premise/premise_home')
const controllerGenre = require('./controller/genre/genre_home')
const controllerCheckbox = require('./controller/checkbox/checkbox_home')
const controllerLockLine = require('./controller/lock_line/lock_line_home')
const Module = require('./function_global')
const controllerTheme = require('./controller/theme/theme_home')
const controllerLockstoryline = require('./controller/lock_story_line/lock_story_line')
const controllerCharecter = require('./controller/character/character_home')
const controllerPlot = require('./controller/act/act_home')
const controllerStoryBow = require('./controller/story_bow/story_bow_home')
// import { v4 as uuidv4 } from 'uuid';
var cors = require('cors');
const multer = require("multer");
var uuid = require('uuid');

// const upload = multer({ dest: "uploads/" });


// var upload = multer({ storage: storage })
function getRouter(app){
    //  setting-start
    var sess = 100;



    app.get('/api',controllerLogin.test_api);
    app.post('/api/login',controllerLogin.login);
    app.post('/api/protected',Module.ensureToken,controllerLogin.protected);
    app.post('/register',controllerRegister.save_member);
    app.post('/project/list',Module.ensureToken,controllerProject.get_project);
    app.post('/project/title',Module.ensureToken,controllerProject.set_title);
    app.post('/board/list',Module.ensureToken,controllerBoard.get_all_project);
    
    app.post('/premise/text',Module.ensureToken,controllerPremise.set_premise_txt);
    app.post('/premise/honeycomb',Module.ensureToken,controllerPremise.honeycomb_save);
    app.post('/premise/get_honeycomb',Module.ensureToken,controllerPremise.get_honeycomb);
    app.post('/premise/save_card',Module.ensureToken,controllerPremise.save_card_premise);
    app.post('/premise/get_card_premise',Module.ensureToken,controllerPremise.get_card_premise);
    app.post('/premise/update_card',Module.ensureToken,controllerPremise.update_card);
    app.post('/premise/update_card_premise_project',Module.ensureToken,controllerPremise.update_card_premise_project);
    app.post('/premise/delete_card_premise',Module.ensureToken,controllerPremise.delete_card_premise);
    
    
    

    app.post('/theme/get_card_premise_my',Module.ensureToken,controllerTheme.get_card_premise_my);
    app.post('/theme/create',Module.ensureToken,controllerTheme.add_theme);
    app.post('/theme/get',Module.ensureToken,controllerTheme.get_theme);
    app.post('/theme/update',Module.ensureToken,controllerTheme.update_theme);
    app.post('/theme/update_check_theme',Module.ensureToken,controllerTheme.update_check_theme);
    app.post('/theme/update_final_theme_name',Module.ensureToken,controllerTheme.update_final_theme_name);
    app.post('/theme/my_card_current_theme',Module.ensureToken,controllerTheme.my_card_current_theme);
    
    
    app.post('/genre/text',Module.ensureToken,controllerGenre.set_genre_txt);
    app.post('/checkbox/text',Module.ensureToken,controllerCheckbox.set_checkbox_txt);
    app.post('/lock_line/text',Module.ensureToken,controllerLockLine.set_lock_line_txt);
    app.post('/lock_story_line/lock_story_line_add',Module.ensureToken,controllerLockstoryline.lock_story_line_add);
    app.post('/lock_story_line/get_lock_story_line',Module.ensureToken,controllerLockstoryline.get_lock_story_line);
    app.post('/lock_story_line/get_lock_story_line_all',Module.ensureToken,controllerLockstoryline.get_lock_story_line_all);



    app.post('/character/set_name',Module.ensureToken,controllerCharecter.set_name);
    app.post('/character/get_character',Module.ensureToken,controllerCharecter.get_character);
    app.post('/character/set_popup',Module.ensureToken,controllerCharecter.set_popup);
    app.post('/character/save_pov',Module.ensureToken,controllerCharecter.save_pov);
    app.post('/character/save_theme',Module.ensureToken,controllerCharecter.save_theme);
    app.post('/character/confirm_summary',Module.ensureToken,controllerCharecter.confirm_summary);
    app.post('/character/get_character_all',Module.ensureToken,controllerCharecter.get_character_all);
    app.post('/character/upload_file',Module.ensureToken,controllerCharecter.upload_file);
    app.post('/character/get_file',Module.ensureToken,controllerCharecter.get_file);
    app.post('/character/get_home_lock_story_line',Module.ensureToken,controllerCharecter.get_home_lock_story_line);

    app.post('/plot/add_plot',Module.ensureToken,controllerPlot.add_plot);
    app.post('/plot/get_plot',Module.ensureToken,controllerPlot.get_plot);

    app.post('/story_bow/add_card_story_bowl',Module.ensureToken,controllerStoryBow.add_card_story_bowl);
    app.post('/story_bow/get_card_story_bow',Module.ensureToken,controllerStoryBow.get_card_story_bow);
    app.post('/story_bow/delete_card',Module.ensureToken,controllerStoryBow.delete_card);
    app.post('/story_bow/show_story_bow',Module.ensureToken,controllerStoryBow.show_story_bow);
    app.post('/story_bow/get_show_story_bow',Module.ensureToken,controllerStoryBow.get_show_story_bow);
    app.post('/story_bow/update_plot_full',Module.ensureToken,controllerStoryBow.update_plot_full);

    
    

    




    app.post('/lock_story_line/random_sigle_active',Module.ensureToken,controllerLockstoryline.random_sigle_active);
    app.post('/lock_story_line/random_all_active',Module.ensureToken,controllerLockstoryline.random_all_active);
    
    
    // app.post('/project/list',async function(req,res){
    //     await res.json({
    //         data:"data"
    //     })
    // });

    app.post('/fm_data', (req, res) => {
            console.log(req.body,'body');
            console.log(req.query,'query');
        res.json({
            status:true
        })

        // console.log('single');
        // try {
        //   res.send(req.file);
        // }catch(err) {
        //   res.send(400);
        // }
    });

    app.post('/single', (req, res) => {
        console.log('single');
        try {
          res.send(req.file);
        }catch(err) {
          res.send(400);
        }
    });

    app.post("/send", (req, res) => {
        // console.log('SEND');
        res.cookie('t1',100);
        res.json({
            data:"json"
        });

    });

    app.post('/read',function(req,res){
        // console.log(req.session,'read');
        console.log(req.cookies.t1);
        res.json({
            data:"message"
        })
    });

}
module.exports = {
    getRouter
}