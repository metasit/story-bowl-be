var express = require('express');
var app    = express();
let mysql = require('mysql');
var cors = require('cors');
var db = require('../../connect_db');
app.use(cors())
var jwt = require('jsonwebtoken');
const { json } = require('body-parser');
const multer = require("multer");
const { response } = require('express');
app.use(express.urlencoded({ extended: true }))
app.use(express.json()) 

const StoryBowl = {
    async add_card_story_bowl(req,res){
        
        let { project_id,number_card,content_card,status_title,color_card} = req.body
        let special
        if(req.body.special){
            special = req.body.special
        }

        console.log(project_id,'project_id');
        console.log(number_card,'number_card');
        console.log(content_card,'content_card');
        console.log(status_title,'status_title');
        console.log(color_card,'color_card');

        // 


        let sql_list = ` SELECT * FROM story_bow WHERE project_id = '${project_id}' AND number_card = '${number_card}'  `;
        let sql_result = await db.con_db(sql_list);

        if(sql_result.length == 0){
            let sql_insert_new
            if([3,4,6,13,22,24].indexOf(number_card) >= 0){
                sql_insert_new =  
                ` 
                INSERT INTO story_bow (content_card,status_title,status_card,number_card,color_card,project_id,special_status) VALUES
                ('${content_card}','${status_title}','ACTIVE','${number_card}','${color_card}','${project_id}','${special}')
                `
            }
            else {
                sql_insert_new =  
                ` 
                INSERT INTO story_bow (content_card,status_title,status_card,number_card,color_card,project_id) VALUES
                ('${content_card}','${status_title}','ACTIVE','${number_card}','${color_card}','${project_id}')
                `
            }
            
            let sql_insert_new_result = await db.con_db(sql_insert_new);
            if(sql_insert_new_result){
               res.json({
                status:true
               }) 
            }
            else {
                res.json({
                    status:false,
                    sql:sql_insert_new
                })
            }
        }
        else{

            let sql_update_card
            if([3,4,6,13,22,24].indexOf(parseInt(number_card)) >= 0){
                sql_update_card = ` UPDATE  story_bow SET 
                content_card = '${content_card}' , 
                status_card = 'ACTIVE', 
                special_status = '${special}'
                WHERE project_id = '${project_id}' AND number_card = '${number_card}'   `
                console.log(sql_update_card,'sql_update_card');
            }
            else {
                sql_update_card = ` UPDATE  story_bow SET content_card = '${content_card}' , status_card = 'ACTIVE' WHERE project_id = '${project_id}' AND number_card = '${number_card}'   `
            }
             
            let sql_update_card_result = await db.con_db(sql_update_card);
            if(sql_update_card_result){
                res.json({
                    status:true
                })
            }
            else {
                res.json({
                    status:false,
                    sql:sql_update_card
                })
            }
        }
    },
    async get_card_story_bow(req,res){
        let { project_id } = req.body;
        let sql_list_card = ` SELECT * FROM story_bow WHERE project_id = '${project_id}' order by number_card asc  `;
        let sql_list_card_result = await db.con_db(sql_list_card);
        if(sql_list_card_result){
            res.json({
                status:true,
                data:sql_list_card_result
            })
        }
        else{
            res.json({
                status:false
            })
        }
    },
    async delete_card(req,res){
        let { project_id , number }  = req.body
        let sql_ck = ` SELECT * FROM story_bow WHERE project_id = '${project_id}' AND  number_card = ${number} `;
        let sql_ck_result = await db.con_db(sql_ck);
        if(sql_ck_result.length == 0){
             let sql_insert = ` INSERT INTO story_bow (status_card,number_card, project_id) VALUES('','${number}','${project_id}') `;
             let sql_insert_result = await db.con_db(sql_insert);
             if(sql_insert_result){
                 res.json({
                     status:true
                 })
             }
             else{
                 res.json({
                     status:false,
                     sql:sql_insert
                 })
             }
        }
        else {
            let sql_update = `  UPDATE story_bow SET  	status_card = '' WHERE project_id = '${project_id}' AND number_card = '${number}'  `;
            let sql_update_result = await db.con_db(sql_update);
            if(sql_update_result){
                res.json({
                    status:true
                })
            }
            else{
                res.json({
                    status:false,
                    sql:sql_update
                })
            }
        }
    },
    async show_story_bow(req,res){
        let { project_id ,field,status } = req.body
    
        let sql_list = ` SELECT * FROM show_story_bow WHERE project_id = '${project_id}' `;
        let sql_list_result = await db.con_db(sql_list);
        if(sql_list_result.length == 0){
            let sql_insert = `  INSERT INTO show_story_bow  (${field},project_id)   VALUES  ('${status}','${project_id}')   `;
            let sql_insert_result = await db.con_db(sql_insert);
            if(sql_insert_result){
                res.json({
                    status:true
                })
            }
            else{
                res.json({
                    status:false,
                    sql:sql_insert
                })
            }
        }
        else {
            
            let sql_update = ` UPDATE show_story_bow SET ${field} = '${status}' WHERE project_id = '${project_id}'    `
            console.log(sql_update,'sql_update');
            let sql_update_result = await db.con_db(sql_update);
            if(sql_update_result){
                res.json({
                    status:true
                })
            }
            else{
                res.json({
                    status:false,
                    sql:sql_update
                })
            }
        }
    },
    async get_show_story_bow(req,res){
        let { project_id } = req.body
        let sql_list = ` SELECT * FROM show_story_bow WHERE project_id = '${project_id}' `
        let sql_list_result = await db.con_db(sql_list);
        console.log(sql_list,'sql_list');
        if(sql_list_result){
            res.json({
                status:true,
                data:sql_list_result
            })
        }
        else {
            res.json({
                status:false,
                sql:sql_list
            })
        }
    },
    async update_plot_full(req,res){

        
        let { project_id,act1,act2,act3 } = req.body 
        let update_plot = ` 
        UPDATE act SET 
        act_name1 = '${act1}' , 
        act_name2 ='${act2}' , 
        act_name3 = '${act3}' WHERE project_id = '${project_id}'  `
        let update_plot_result = await db.con_db(update_plot);
        console.log(update_plot,'update_plot');
        if(update_plot_result){ 
            res.json({
                status:true
            })
         }
        else {
            res.json({
                status:false,
                sql:update_plot
            })
        }


    }

}
module.exports = StoryBowl