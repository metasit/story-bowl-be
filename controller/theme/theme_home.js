var express = require('express');
var app    = express();
let mysql = require('mysql');
var cors = require('cors');
var db = require('../../connect_db');
app.use(cors())
var jwt = require('jsonwebtoken');
const { get_card_premise } = require('../premise/premise_home');
const Theme = {


    async get_theme(req,res){
        let { project_id } = req.body 
        let sql_list = ` SELECT * FROM theme WHERE project_id = '${project_id}'  `;
        let sql_query =  await  db.con_db(sql_list);
        if(sql_query){
            res.json({
                status:true,
                data:sql_query
            })
        }   
        else{
            res.json({
                status:false,
                sql:sql_list
            })
        }
    },

    async add_theme(req,res){
        // if(status == "-1"){
            let { status,name_theme,project_id ,blog } = req.body
            let sql_inset_theme = `
            INSERT INTO theme (id_theme, status_theme, name_theme, blog, project_id, create_at, update_at) 
            VALUES (NULL, '${status}', '${name_theme}','${blog}' ,'${project_id}', 'current_timestamp()', 'current_timestamp()');
            `
            // console.log(sql_inset_project,'sql_inset_project');
            let sql_query =  await  db.con_db(sql_inset_theme);   
            
            
            if(sql_query.affectedRows >=1){
                res.json({
                    status:true,
                    id_project:sql_query.insertId
                })
                return
            }
            else{
                res.json({
                    status:false,
                    message:"Data Error"
                })
            }
        // }
    },
    async update_theme(req,res){
        let { project_id ,name ,id_theme } = req.body
        $sql_update = `  UPDATE theme set name_theme = '${name}' WHERE project_id =  '${project_id}' AND id_theme = '${id_theme}' `;
        let sql_query =  await  db.con_db($sql_update);   
        if(sql_query.affectedRows >=1){
            res.json({
                status:true,
                // id_project:sql_query.insertId
            })
            return
        }
        else{
            res.json({
                status:false,
                message:"Data Error"
            })
        }
    },
    async update_check_theme(req,res){
        let { id,project_id } = req.body 
        $sql_select_check =  ` SELECT * FROM theme WHERE id_theme = '${id}'  AND project_id = ${project_id}  `;
        let sql_query_check =  await  db.con_db($sql_select_check);
        // console.log(sql_query_check);
        let st_check =  sql_query_check[0].status_check
        console.log(st_check,'st_check');
        console.log(typeof st_check);
        // console.log(st_check.length,'length');
        // console.log(st_check == null,'st');
        if(st_check == null  || st_check == ""  || st_check == "FALSE"){
            $sql_update = ` UPDATE theme SET status_check = 'TRUE' WHERE project_id = '${project_id}' AND id_theme = '${id}' `
        }
        else {
            $sql_update = ` UPDATE theme SET status_check = 'FALSE' WHERE project_id = '${project_id}' AND id_theme = '${id}' `
        }
        let sql_query_check_confirm =  await  db.con_db($sql_update);
        if(sql_query_check_confirm){
            res.json({
                status:true
            })
        }
        else {
            res.json({
                status:false
            })
        }
        // 
    },
    async get_card_premise_my(req,res){
        let {id_card_premise,project_id} =  req.body;
        let sql_premise = `
        SELECT * FROM project as pj 
        LEFT JOIN card_premise as cp ON pj.premise_select_card = cp.id_card_premise 
        WHERE pj.id_project = '${project_id}' 
        `;
        // console.log($sql_premise,'sql_premise');
        sql_query = await  db.con_db(sql_premise);
        // console.log($sql_query);
        if(sql_query){
            res.json({
                status:true,
                data:sql_query
            })
        }
        else {
            res.json({
                status:false
            })
        }
    },
    async update_final_theme_name(req,res){
        let { name_theme , project_id } = req.body;
        let sql_update = `UPDATE project  SET theme_name = '${name_theme}' WHERE id_project = ${project_id}`;
        let sql_query  = await  db.con_db(sql_update);
        console.log(sql_update,'sql_update');
        if(sql_query){
            res.json({
                status:true,
                // data:sql_query
            })
        }
        else {
            res.json({
                status:false
            })
        }
    },
    async my_card_current_theme(req,res){
        let { project_id , order_id } = req.body ;
        let sql_update = ` UPDATE project set 	theme_select_card = '${order_id}' WHERE  id_project = '${project_id}'`;
        let sql_query  = await  db.con_db(sql_update);
        if(sql_query){
            res.json({
                status:true,
                // data:sql_query
            })
        }
        else {
            res.json({
                status:false
            })
        }
    }    

}
module.exports = Theme

// https://www.youtube.com/embed/Q33KBiDriJY
