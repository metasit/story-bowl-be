var express = require('express');
var app    = express();
let mysql = require('mysql');
var cors = require('cors');
var db = require('../../connect_db');
app.use(cors())
var jwt = require('jsonwebtoken');
const { json } = require('body-parser');
var formidable = require('formidable');
var fs = require('fs');
const multer = require("multer");
app.use(express.urlencoded({ extended: true }))
app.use(express.json()) 
var uuid = require('uuid');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './public/image')
    },
    filename: function (req, file, cb) {
        cb(null, uuid.v4()+"."+file.originalname.split(".")[1])
    }
})
var upload = multer({ storage : storage }).array('profile',100);

const Character = {
    async get_character(req,res){
            let { project_id,character_id } = req.body;
            let sql_list = ` SELECT * FROM charecter WHERE project_id =  '${project_id}'  AND id_charecter  = '${character_id}'  `
            let sql_query = await db.con_db(sql_list);
            if(sql_query){
                res.json({
                    status:true,
                    data:sql_query,
                    sql:sql_list
                })
            }
            else {
                res.json({
                    status:false,
                })
            }
    },
    async get_character_all(req,res){
        let { project_id,character_id } = req.body;
        let sql_list = ` SELECT * FROM charecter WHERE project_id =  '${project_id}'  `
        let sql_query = await db.con_db(sql_list);
        if(sql_query){
            res.json({
                status:true,
                data:sql_query,
                sql:sql_list
            })
        }
        else {
            res.json({
                status:false,
            })
        }
},

    async set_name(req,res){
        let { project_id,name,character_id } = req.body;
        let sql_list = ` SELECT * FROM charecter WHERE project_id = '${project_id}' AND id_charecter =  '${character_id}'  `;
        let sql_result =  await db.con_db(sql_list);
        // console.log(sql_list,'sql_list');
        if (sql_result.length==0) {
           let sql_name =  `
                INSERT INTO charecter (id_charecter, name_charecter, image_charecter, work, bg_work, home, bg_home, play, bg_play, bio, bg_bio, psycho, bg_psycho, social, bg_social, hp, bg_hp, wp, wp_bg, pov, dramtic_need, dramtic_need_bg, poi, poi_bg, kind, theme, project_id, create_at, update_at) 
                VALUES (NULL, '${name}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
                NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '16', '', '');
               
            `
            // console.log(sql_name,'sql_name');
            let sql_query_name =  await db.con_db(sql_name);
            // return
            if(sql_query_name){
                res.json({
                    status:true,
                    id_project:sql_query_name.insertId
                })
            }
            else {
                res.json({
                    status:false,
                    sql:sql_name
                })
            }
        }
        else {
            let sql_update = `  UPDATE charecter set name_charecter = '${name}'   WHERE  project_id = '${project_id}' AND id_charecter = '${character_id}'  `;
            let sql_update_query = await db.con_db(sql_update);
            console.log(sql_update_query,'sql_update_query');
            if(sql_update_query){
                res.json({
                    status:true,
                    id_project:character_id
                })
            }
            else {
                res.json({
                    status:false
                })
            }
        }
    },
    async set_popup(req,res){

        let { content,bg_content,field,project_id,character_id } = req.body
        let f1 = field.split("/")[0]+ " = " + `'${content}'`;
        let f2 = field.split("/")[1]+ " = "+ `'${bg_content}' `

        let f1_name =  field.split("/")[0];
        let f2_name = field.split("/")[1];

        let sql_list = ` SELECT * FROM charecter WHERE project_id = '${project_id}' AND  id_charecter = '${character_id}'  `;
        let sql_result =  await db.con_db(sql_list);
        // console.log(sql_list);
        // return

        if (sql_result.length==0){
           
            let sql_insert = `INSERT INTO charecter (${f1_name},${f2_name} , project_id) values('${content}','${bg_content}' ,'${project_id}' )`
            let sql_inset_result = await db.con_db(sql_insert)
            if(sql_inset_result){
                res.json({
                    status:true,
                    character_id:sql_inset_result.insertId  
                })
            }
            else {
                res.json({
                    status:false,
                    sql:sql_insert
                })
            }
        }
        else {
            let sql_update = ` UPDATE charecter SET  ${f1} ,  ${f2} WHERE project_id = '${project_id}' AND id_charecter = '${character_id}'  `;
            let sql_update_result = await db.con_db(sql_update)
            if(sql_update_result){
                res.json({
                    status:true,
                    character_id:character_id
                })
            }
            else {
                res.json({
                    status:false,
                    sql:sql_update_result
                })
             }
        }
    },
    async save_pov(req,res){
        
        let { project_id,name_pov,character_id } = req.body;
        let sql_list = ` SELECT * FROM charecter WHERE project_id = '${project_id}' AND id_charecter = '${character_id}'  `;
        let sql_result =  await db.con_db(sql_list);
        // console.log(sql_list,'sql_list');
        // return

        if (sql_result.length==0) {
            let sql_insert =  `INSERT INTO charecter (pov,project_id) values('${name_pov}','${project_id}')`;
            let sql_insert_query = await db.con_db(sql_insert);
            if(sql_insert_query){
                res.json({
                    status:true,
                    character_id:sql_insert_query.insertId 
                    
                })
            }
            else {
                res.json({
                    status:false,
                    sql:sql_insert
                })
            }
        }
        else {
            let sql_update = `UPDATE charecter SET pov = '${name_pov}' WHERE project_id = '${project_id}' AND id_charecter = '${character_id}' `;    
            let sql_update_query  = await db.con_db(sql_update)
            if(sql_update_query){
                res.json({
                    status:true,
                    character_id:character_id
                })
            }
            else {
                res.json({
                    status:false,
                    sql:sql_update
                })
            }
        }

    },


    async save_theme(req,res){
        let { project_id,name_theme , character_id } = req.body;
        let sql_list = ` SELECT * FROM charecter WHERE project_id = '${project_id}' AND id_charecter = '${character_id}'  `;
        let sql_result =  await db.con_db(sql_list);
        // console.log(name_theme,'name_theme');
        // return
        if (sql_result.length==0) {
            let sql_insert =  `INSERT INTO charecter (theme,project_id) values('${name_theme}','${project_id}')`;
            let sql_insert_query = await db.con_db(sql_insert);
            if(sql_insert_query){
                res.json({
                    status:true,
                    character_id:sql_insert_query.insertId 
                })
            }
            else {
                res.json({
                    status:false,
                    sql:sql_insert
                })
            }
        }
        else {
            let sql_update = `UPDATE charecter SET theme = '${name_theme}' WHERE project_id = '${project_id}' AND id_charecter = '${character_id}'`;    
            let sql_update_query  = await db.con_db(sql_update)
            if(sql_update_query){
                res.json({
                    status:true,
                    character_id:character_id
                })
            }
            else {
                res.json({
                    status:false,
                    sql:sql_update
                })
            }
        }
    },
    async confirm_summary(req,res){
        let { project_id,name_have } = req.body
        let sql_clear_active = ` UPDATE lock_story_line SET status_select = '' 
        WHERE project_id = '${project_id}' AND type_lock_story_line_id = '${4}'
        `
        let sql_clear_query = await db.con_db(sql_clear_active);
        if(sql_clear_query){
            let sql_insert = `
            INSERT INTO lock_story_line (id_lock_story_line, name, status_select, type_lock_story_line_id, project_id, create_at, update_at) 
            VALUES (NULL, '${name_have}', 'ACTIVE', '${4}', '${project_id}', current_timestamp(), current_timestamp());
            `
            let sql_insert_query = await db.con_db(sql_insert);
            if(sql_insert_query){
                res.json({
                    status:true
                })
            }
            else {
                res.json({
                    status:false
                })
            }
        }
    },

    async upload_file(req,res){
        console.log('upload_file');
        // let { project_id,character_id  } = req.body
        // console.log(req.body,'req.body');
        // console.log(project_id,'project_id');
        // console.log(character_id,'character_id');
        
        upload(req,res, async function(err) {
            // console.log(req.body);
            let { project_id , character_id , order_file } = req.body
            let let_arr_file = [];
            let let_arr_file_token = [];
            let select_old =  `SELECT * FROM  image_character WHERE project_id = '${project_id}' AND character_id = '${character_id}'`
            let select_old_query = await db.con_db(select_old)

            if(select_old_query.length>0){
                for (let x = 0; x < select_old_query.length; x++) {
                    let value = select_old_query[x]['name_file'];
                    // let_arr_file.push(value);
                    let_arr_file_token.push(value)
                }
            }

            for (let x = 0; x < req.files.length; x++) {
                const file = req.files[x];
                let_arr_file.push(file.filename);
                let_arr_file_token.push(file.filename)
                // console.log(file.filename,'filename');                
            }



        
            // console.log(project_id,'project_id');
            // console.log(character_id,'character');
            
            let sql_list = ` SELECT * FROM charecter WHERE project_id = '${project_id}' AND id_charecter = '${character_id}'  `;
            let sql_result =  await db.con_db(sql_list);
            
            if (sql_result.length==0){
                let file_select = let_arr_file[order_file];
                let sql_insert_file = ` INSERT INTO  charecter (image_charecter,project_id) VALUES ('${file_select}','${project_id}')  `
                let sql_insert_result = await db.con_db(sql_insert_file);
                console.log(sql_insert_result,'sql_insert_file');

                if(sql_insert_result){
                    
                    let delete_photo_all = ` DELETE FROM image_character WHERE project_id = '${project_id}' AND  character_id = '${character_id}'`;
                    let delete_photo_result = await db.con_db(delete_photo_all);
                    
                    if(delete_photo_result){
                            for (let x = 0; x < let_arr_file.length; x++) {
                                const file_value = let_arr_file[x];
                                let insert_file_map = ` INSERT INTO image_character (name_file,character_id,project_id) VALUES('${file_value}','${sql_insert_result.insertId}','${project_id}') `;
                                // console.log(insert_file_map,'insert_file_map');
                                let insert_file_map_result = await db.con_db(insert_file_map);
                            }
                            res.json({
                                status:true,
                                character_id:sql_insert_result.insertId 
                            })
                    }
                    else{
                        res.json({
                            status:false,
                            sql:delete_photo_all
                        })
                    }

                    

                    
                    // res.json({
                    //     status:true,
                    //     character_id:sql_insert_result.insertId
                    // })

                }
                else {
                    res.json({
                        status:false,
                        sql:sql_insert_file
                    })
                }
            }
            else {
                // console.log(let_arr_file,'let_arr_file');
                if(req.files.length==0) {
                    let arr_file = []
                    let select_file = ` SELECT * FROM  image_character WHERE project_id = '${project_id}' AND  	character_id = '${character_id}'`;
                    // console.log(select_file,'select_file');
                    let select_file_result = await db.con_db(select_file);
                    for (let x = 0; x < select_file_result.length; x++) {
                        const element = select_file_result[x];
                        arr_file.push(element.name_file)
                    }
                    console.log(arr_file,'arr_file1111');
                    console.log(order_file,'order_file');

                    var sql_update = `UPDATE charecter SET image_charecter = '${arr_file[order_file]}' WHERE project_id = '${project_id}' AND id_charecter  = '${character_id}'`;
                    console.log(sql_update,'sql_update');

                }
                else {
                    // let delete_photo_all = ` DELETE FROM image_character WHERE project_id = '${project_id}' AND  character_id = '${character_id}'`;
                    // let delete_photo_result = await db.con_db(delete_photo_all);
                    let file_select = let_arr_file_token[order_file];
                    // if(delete_photo_result){
                        for (let x = 0; x < let_arr_file.length; x++) {
                            const file_value = let_arr_file[x];
                            let insert_file_map = ` INSERT INTO image_character (name_file,character_id,project_id) VALUES('${file_value}','${character_id}','${project_id}') `;
                            let insert_file_map_result = await db.con_db(insert_file_map);
                        }
                //   }
                    console.log(file_select,'file_select');
                    console.log(let_arr_file,'let_arr_file');
                    var sql_update = `UPDATE charecter SET image_charecter = '${file_select}' WHERE project_id = '${project_id}' AND id_charecter  = '${character_id}'`;
                    // console.log(sql_update,'sql_update');
                    
                }
                let sql_update_result = await db.con_db(sql_update)



                if(sql_update_result){
                    res.json({
                        status:true,
                        character_id:character_id
                    })
                }
                else{
                    res.json({
                        status:false,
                        sql:sql_update
                    })
                }
            }
            if(err) {
                return res.end("Error uploading file.");
            }
        });



        // // if (req.url == '/fileupload') {
        //     var form = new formidable.IncomingForm();        
        // // }
    },

    async get_file(req,res){
        let { project_id,character_id } = req.body
        let sql_list = `SELECT * FROM image_character WHERE project_id = '${project_id}' AND character_id ='${character_id}'`;
        console.log(sql_list,'sql_list');
        let sql_result = await db.con_db(sql_list);

        if(sql_result){
            res.json({
                status:true,
                data:sql_result
            })
        }
        else {
            res.json({
                status:false,
                sql:sql_list
            })
        }
    },

    async get_home_lock_story_line(req,res){

        let { project_id } =  req.body
        let sql_list = `SELECT * FROM lock_story_line WHERE project_id = '${project_id}' AND status_select = 'ACTIVE'  order by type_lock_story_line_id asc `
        let sql_result = await db.con_db(sql_list);
        if(sql_result){
            res.json({
                status:true,
                data:sql_result
            })
        }
        else {
            res.json({
                status:false,
                sql:sql_list
            })
        }

    }

  
}
module.exports = Character
