var express = require('express');
var app    = express();
let mysql = require('mysql');
var cors = require('cors');
var db = require('../../connect_db');
app.use(cors())
var jwt = require('jsonwebtoken');
const { json } = require('body-parser');
const e = require('express');
const LockStoryLine = {

    async lock_story_line_add (req,res){
        let { data_card,project_id,type_id,order } = req.body
        let sql_delete_txt = ` DELETE FROM lock_story_line  WHERE project_id = '${project_id}' AND type_lock_story_line_id = '${type_id}'  `;
        let sql_delete =  await  db.con_db(sql_delete_txt);
        if(sql_delete) {
                for (let x = 0; x < data_card.length; x++) {
                    const value = data_card[x];
                    let sql_inset_card
                    if(x== order) {
                        sql_inset_card = ` 
                        INSERT INTO lock_story_line (id_lock_story_line, name, status_select, type_lock_story_line_id, project_id, create_at, update_at) 
                        VALUES (NULL, '${value}', 'ACTIVE', '${type_id}', '${project_id}', current_timestamp(), current_timestamp());
                        `
                    }
                    else {
                        sql_inset_card = ` 
                        INSERT INTO lock_story_line (id_lock_story_line, name, status_select, type_lock_story_line_id, project_id, create_at, update_at) 
                        VALUES (NULL, '${value}', '', '${type_id}', '${project_id}', current_timestamp(), current_timestamp());
                        `
                    }

                    let sql_insert_query =  await  db.con_db(sql_inset_card);                     
                }
                // let update_bow = ` UPDATE story_bow SET   `


        }   
        else {
            res.json({
                status:false,
                sql:sql_delete_txt
            })
        }
        res.json({
            status:true
        })
    },
    async get_lock_story_line(req,res){
        let { type_id,project_id } = req.body;
        let sql_list = `SELECT * FROM lock_story_line WHERE type_lock_story_line_id = '${type_id}' AND project_id = '${project_id}'`;
        let sql_result =  await db.con_db(sql_list);
        if (sql_result) {
            res.json({
                data:sql_result,
                status:true
            })
        }
        else {
            res.json({
                status:false,
                sql:sql_list
            })
        }
    },
    async get_lock_story_line_all(req,res){
        let  { project_id } = req.body 
        let sql_list = ` SELECT * FROM lock_story_line WHERE project_id = '${project_id}' AND status_select = 'ACTIVE'  `;
        let sql_result = await db.con_db(sql_list);
        
        if(sql_result){
            res.json({
                data:sql_result,
                status:true
            })
        }
        else {
            res.json({
                status:false,
            })
        }
    },
    async random_sigle_active(req,res){
         let { project_id , type_id  } = req.body;
         let sql_update = `UPDATE  lock_story_line SET status_select = '' WHERE project_id = '${project_id}'  AND  	type_lock_story_line_id = '${type_id}' `;
         let result_rest = await db.con_db(sql_update);
         if(result_rest){
             $sql_update_random = ` UPDATE lock_story_line SET status_select = 'ACTIVE'
             WHERE project_id = '${project_id}' AND type_lock_story_line_id = '${type_id}' ORDER BY RAND() limit 1   `;
             $result_random = await db.con_db($sql_update_random);
             if($result_random){
                 res.json({
                     status:true,
                 })
             }
         }
         else {
            res.json({
                status:false,
                message:"reset",
                mysql:"sql_update"
            })
         }
    },

    async random_all_active(req,res){
        let { project_id  } = req.body;
        let sql_update = `UPDATE  lock_story_line SET status_select = '' WHERE project_id = '${project_id}'   `;
        let result_rest = await db.con_db(sql_update);
        if(result_rest){
            console.log('Clear_success');
            for (let x = 1; x <= 7; x++) {
                console.log(x,'xxxx');
                $sql_update_random = ` 
                UPDATE lock_story_line SET status_select = 'ACTIVE'
                WHERE project_id = '${project_id}' 
                AND type_lock_story_line_id = '${x}' ORDER BY RAND() limit 1   `;
                await db.con_db($sql_update_random);
                
            }
            res.json({
                status:true,
            })
        }
        else {
           res.json({
               status:false,
               message:"reset",
               mysql:"sql_update"
           })
        }
   },
   
}
module.exports = LockStoryLine
