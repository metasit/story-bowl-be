var express = require('express');
var app    = express();
let mysql = require('mysql');
var cors = require('cors');
var db = require('../../connect_db');
app.use(cors())
var jwt = require('jsonwebtoken');
const Premise = {

    async set_premise_txt(req,res){
        let { txt,status,id_member } = req.body;
        if(status == "-1"){
            let sql_inset_project = `
            INSERT INTO project (
                    id_project, name_project, title_coverage, genre, theme_name, lock_story_line_name, premise_name, id_member, create_at, update_at) 
            VALUES  (NULL, '', NULL, NULL, NULL, NULL, '${txt}',  '${id_member}', current_timestamp(), current_timestamp());`
            console.log(sql_inset_project,'sql_inset_project');
            let sql_query =  await  db.con_db(sql_inset_project);            
            if(sql_query.affectedRows >=1){
                res.json({
                    status:true,
                    id_project:sql_query.insertId
                })
                return
            }
            else{
                res.json({
                    status:false,
                    message:"Data Error"
                })
            }
        }
        else {
            let sql_update_project = ` UPDATE project set premise_name = '${txt}' WHERE id_project = '${status}' `
            let sql_query =  await  db.con_db(sql_update_project);
            if(sql_query != false && sql_query.affectedRows  >=1){
                let list_sql = await db.con_db(` SELECT * FROM  project WHERE id_project = '${status}' `)
                res.json({
                    data:list_sql,
                    status:true
                })
            }

        }
    },

    async honeycomb_save(req,res){
        let { name,color,position,project_id }  = req.body 
        let select_premise = ` SELECT * FROM premise WHERE position = '${position}'  AND project_id = '${project_id}'   `;        
        let sql_query_select =  await  db.con_db(select_premise);
        console.log(sql_query_select,'sql_query_select');
        

        if(sql_query_select.length == 0) {
            let sql_insert = `
            INSERT INTO premise (id_premise, name, color, position, project_id, create_at, update_at) 
            VALUES (NULL, '${name}', '${color}', '${position}', '${project_id}', current_timestamp(), current_timestamp());
            `
            let sql_query =  await  db.con_db(sql_insert);            
            if(sql_query.affectedRows >=1){
                res.json({
                    status:true,
                    id_project:sql_query.insertId
                })
                return
            }
            else{
                res.json({
                    status:false,
                    message:"Data Error"
                })
            }
        }
        else {
            let sql_update = `  UPDATE premise set  name =  ${name} , color = ${color} WHERE project_id  = '${project_id}'  `;
            let sql_query =  await  db.con_db(sql_insert);            
            if(sql_query.affectedRows >=1){
                res.json({
                    status:true,
                    id_project:sql_query.insertId
                })
                return
            }
            else{
                res.json({
                    status:false,
                    message:"Data Error"
                })
            }
        }
    },
    async get_honeycomb(req,res){
            let { project_id } = req.body
            let sql_list = ` SELECT * FROM premise WHERE project_id = '${project_id}' `;
            let sql_query =  await  db.con_db(sql_list);  
            res.json({
                data:sql_query
            })
    },

    async get_card_premise(req,res){
        let { project_id } = req.body
        let sql_list = `SELECT * FROM card_premise WHERE project_id = ${project_id} `;
        let sql_query =  await  db.con_db(sql_list);
        if(sql_query){
            res.json({
                data:sql_query,
                status:true
            })
        }
        else {
            res.json({
                data:sql_list,
                status:false
            })
        }

    },
    async save_card_premise(req,res){
        let { project_id } = req.body
        let sql_insert_card = `   
            INSERT INTO card_premise (id_card_premise, content1, title_content2, content2, title_content3, content3, project_id, create_at, update_at) 
            VALUES (NULL, NULL, NULL, NULL, NULL, NULL, '${project_id}', current_timestamp(), current_timestamp());
        `
        let sql_query =  await  db.con_db(sql_insert_card);  
        let sql_list = ` SELECT * FROM card_premise  WHERE project_id = '${project_id}'  `
        let sql_list_query = await db.con_db(sql_list);
        // console.log(sql_list_query);
        if(sql_insert_card){
            res.json({
                data:sql_list_query,
                status:true
            })
        }
        else {
            res.json({
                status:false,
                sql:this.state.sql_insert_card
            })
        }
    },
    async update_card (req,res){
        let { project_id,card_id,status_content,title_content,content } = req.body; 
        let sql_update
        if(status_content == 0){
             sql_update = ` UPDATE card_premise set content1 = '${content}'  WHERE project_id = '${project_id}' AND id_card_premise = '${card_id}'`
        }
        else if(status_content == 1){
            sql_update = ` UPDATE card_premise set  title_content2 = '${title_content}' , content2 = '${content}'   WHERE project_id = '${project_id}' AND id_card_premise = '${card_id}'`
        }
        else if(status_content == 2){
            sql_update = ` UPDATE card_premise set  title_content3 = '${title_content}' , content3 = '${content}'   WHERE project_id = '${project_id}' AND id_card_premise = '${card_id}'`
        }
        let sql_udpate_query = await db.con_db(sql_update);

        if(sql_udpate_query){
            res.json({
                status:true
            })
        }
        else {
            res.json({
                status:false
            })
        }
        // project_id :localStorage.getItem('status_project'),
        // card_id : this.state.number_select,
        // status_content : this.state.status_select,
        // title_content: this.state.title_popup
    },
    async update_card_premise_project  (req,res){
        let {card_id,project_id} = req.body;
        let sql_update_id = `  UPDATE project set premise_select_card = '${card_id}'  WHERE id_project = '${project_id}' `;
        let sql_update_query = await db.con_db(sql_update_id);
        console.log(sql_update_id,'sql_update_id');
        if(sql_update_query){
            res.json({
                status:true
            })
        }
        else {
            res.json({
                status:false
            })
        }
    },
    async delete_card_premise(req,res){ 
        let { card_id,project_id } = req.body;
        // let str = `(${card_id.join(",")})`;
        sql_delete_card = ` DELETE FROM  card_premise WHERE  id_card_premise   =  '${card_id}' AND project_id =  '${project_id}'  `;
        // console.log(sql_delete_card,'sql_delete_card');
        // return
        let sql_update_query = await db.con_db(sql_delete_card);
        console.log(sql_update_query,'sql_update_query');
        // return
        if(sql_update_query.affectedRows>=1){
            res.json({
                status:true
            })
        }
        else {
            res.json({
                status:false,
                sql:'sql_delete_card'
            })
        }

    }
}
module.exports = Premise

// https://www.youtube.com/embed/Q33KBiDriJY
