var express = require('express');
var app    = express();
let mysql = require('mysql');
var cors = require('cors');
var db = require('../../connect_db');
app.use(cors())
var jwt = require('jsonwebtoken');
const { json } = require('body-parser');
const multer = require("multer");
const { response } = require('express');
app.use(express.urlencoded({ extended: true }))
app.use(express.json()) 
// var uuid = require('uuid');
// var storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//       cb(null, './public/image')
//     },
//     filename: function (req, file, cb) {
//         cb(null, uuid.v4()+"."+file.originalname.split(".")[1])
//     }
// })
// var upload = multer({ storage : storage }).array('profile',100);

const Plot = {
    async add_plot(req,res){
            let { project_id ,act ,field } = req.body;
            let sql_list = ` SELECT * FROM act  WHERE project_id = '${project_id}'  `
            let sql_query = await db.con_db(sql_list);
                console.log(project_id,act,field);
            // return

            if(sql_query.length == 0){

                let sql_insert   = `INSERT INTO act (${field},project_id) VALUES('${act}','${project_id}')`;
                let sql_result = await db.con_db(sql_insert)

                if(sql_result){
                    res.json({
                        status:true
                    })
                }
                else{
                    res.json({
                        status:false,
                        sql:sql_insert
                    })
                }
            }
            else{

                let sql_update = ` UPDATE act SET ${field} = '${act}' WHERE project_id = '${project_id}'`;
                let sql_update_result  = await db.con_db(sql_update)
                if(sql_update_result){
                    res.json({
                        status:true
                    })
                }
                else{
                    res.json({
                        status:false,
                        sql:sql_update
                    })
                }

            }
    },
    async get_plot (req,res){
        
        let { project_id } = req.body;
        let sql_list = ` SELECT * FROM act WHERE project_id = '${project_id}' `;
        let sql_result = await db.con_db(sql_list);
        if(sql_result){
            res.json({
                status:true,
                data:sql_result
            })
        }
        else {
            res.json({
                status:false,
                sql:sql_list
            })
        }
    }


}
module.exports = Plot
